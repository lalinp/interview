package Recursion.recursionTests;

import Recursion.BunnyEars;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by LalinPethiyagoda on 29/07/2017.
 */

public class BunnyEarsTest {
    BunnyEars ears;
    @Before
    public void setUp(){
        ears = new BunnyEars();
    }

    @Test
    public void testBunnyEars() {
        int result =ears.bunnyEars(20);
        Assert.assertEquals(40,result);
        int resultZero =ears.bunnyEars(0);
        Assert.assertEquals(0,resultZero);
    }
    @Test
    public void testBunny100Ears() {
        int result =ears.bunnyEars(100);
        Assert.assertEquals(200,result);

    }

    @Test
    public void testBunny1000Ears() {
        int result =ears.bunnyEars(1000);
        Assert.assertEquals(2000,result);

    }

    @Test
    public void testBunnyNoEars() {
        int result =ears.bunnyEars(0);
        Assert.assertEquals(0,result);

    }
}
