package Recursion;

/**
 * Created by LalinPethiyagoda on 06/08/2017.
 */
public class Sixpresent {

    public static void main(String[] args) {
        int[] intArray = new int[] {6};
        System.out.println(array6(intArray,0));
    }


    public static boolean array6(int[] nums, int index) {
        if(nums.length == 0){
            return false;
        }
        if(index >= nums.length){
            return false;
        }

        return nums[index] == 6 || array6(nums,index+1);

    }

}
