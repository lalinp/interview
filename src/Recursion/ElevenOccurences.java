package Recursion;

/**
 * Created by LalinPethiyagoda on 06/08/2017.
 */
public class ElevenOccurences {
    public static void main(String[] args) {
        int[] intArray = new int[] {6,11,11,11};
        System.out.println(array11(intArray,0));
    }

    public static int array11(int[] nums, int index) {
      int counter = 0;
        if(nums.length==0){
            return 0;
        }
        if(index >= nums.length){
            return counter;
        }


        if(nums[index]==11){
            counter++;
        }

        return counter +  array11(nums,index+1);
    }

}
