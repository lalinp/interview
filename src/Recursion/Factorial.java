package Recursion;

import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 27/07/2017.
 */
public class Factorial {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int fibNum = scanner.nextInt();
        System.out.println(fac(fibNum));


    }

    public static int  fac(int x){
        //base case
        if(x==1){
            return 1;
        }

        return x * fac(x-1);
    }
}
