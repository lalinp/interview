package Recursion;

/**
 * Created by LalinPethiyagoda on 30/07/2017.
 */
public class countHi {

    public static void main(String[] args) {
        System.out.println(countHi("dkhidqqhihiwndhidnqndqnhi"));
    }


    public static int countHi(String str) {
        int counter = 0;
        int beginIndex=1;
        if(str==null || str.isEmpty()){
            return 0;
        }
        else if(str.length()== 1){
            return 0;

        }

        if(str.charAt(0)=='h'){
            if(str.charAt(1)=='i') {
                counter++;
                beginIndex=str.indexOf('i')+1;
            }
        }

        return counter +  countHi(str.substring(beginIndex ,str.length()));

    }
}
