package Recursion;

/**
 * Created by LalinPethiyagoda on 27/07/2017.
 */
public class Fibonacci {
    static int prev = 1;
    static int next = 1;
    static int curr =0;

    public static int fibo(int x){

       if(x ==1) {
           return 1;
       }
       else if(x==0){
           return 0;
       }
       else
        return fibo(x-1) + fibo(x-2);

    }

    public static void main(String[] args) {
        System.out.print(fibo(10));
    }
}
