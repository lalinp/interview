package Recursion;

/**
 * Created by LalinPethiyagoda on 01/08/2017.
 */
public class ChangeLetters {
    static StringBuffer changedStr = new StringBuffer() ;
    public static void main(String[] args) {
        System.out.println(changeXY("axsdexwecxxty"));
    }

    public static String changeXY(String str) {

        if(str==null) {
            return "";
        }
        else if(str.length()==0){
            return str;
        }

        return  String.valueOf(str.charAt(0)=='x'?'y':str.charAt(0)) + changeXY(str.substring(1,str.length()));
    }

}
