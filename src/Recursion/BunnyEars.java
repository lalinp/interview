package Recursion;

/**
 * Created by LalinPethiyagoda on 29/07/2017.
 * We have a number of bunnies and each bunny has two big floppy
 * ears.
 * We want to compute the total number of ears
 * across all the bunnies recursively (without loops or multiplication).

 bunnyEars(0) → 0
 bunnyEars(1) → 2
 bunnyEars(2) → 4
 */
public class BunnyEars {

    public static void main(String[] args) {
        System.out.println(bunnyEars(1));
    }


    public static int bunnyEars(int n){
        if( n==0){
            return 0;
        }
        else if( n == 1){
            return 2;
        }
        else
            return 2 + bunnyEars(n-1);
    }
}
