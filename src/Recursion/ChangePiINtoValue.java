package Recursion;

/**
 * Created by LalinPethiyagoda on 01/08/2017.
 */
public class ChangePiINtoValue {


    public static void main(String[] args) {
        System.out.println(changePi("xpipipipizy"));
    }

    public static String changePi(String str) {
        int begIndex =1;
        if(str==null || str.length()==0){
            return "**";
        }
        else if(!str.contains("pi")){
            return str;
        }

        return str.contains("pi")?str.replace("pi","3.14"):"" + changePi(str.substring(1,str.length()));
    }

}
