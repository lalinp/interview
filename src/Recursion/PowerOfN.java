package Recursion;

/**
 * Created by LalinPethiyagoda on 29/07/2017.
 * Task: Given base and n that are both 1 or more,
 * compute recursively (no loops)
 * the value of base to the n power, so powerN(3, 2) is 9 (3 squared).

 */
public class PowerOfN {

    public static void main(String[] args) {
        System.out.println(pwrn(3,4));
    }

    public static int pwrn(int base, int power){
        if( power == 0){

            return 1;
        }

        return 3 * pwrn(base, power-1);


    }
}
