package Recursion;

/**
 * Created by LalinPethiyagoda on 29/07/2017.
 * Given a non-negative int n, return the count of
 * the occurrences of 7 as a digit,
 * so for example 717 yields 2. (no loops).
 * Note that mod (%) by 10 yields the rightmost digit (126 % 10 is 6),
 * while divide (/) by 10 removes the rightmost digit (126 / 10 is 12).

 */
public class CountSeven {

    public static void main(String[] args) {
        System.out.println(occurSeven(2348));
    }

    public static int occurSeven(int n){
        int counter =0;
        if( n == 0){
            return 0;
        }

        if(n%10 == 8){
            counter++;

        }
         return counter + occurSeven(n/10);


    }
}
