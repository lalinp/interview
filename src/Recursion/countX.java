package Recursion;

/**
 * Created by LalinPethiyagoda on 30/07/2017.
 */
public class countX {

    public static void main(String[] args) {

        System.out.println(countX("xxxxhjxjskxhx"));
    }

    public static int countX(String str){
        int counter = 0;
        if(str==null || str.isEmpty()){
            return 0;
        }
        else if(str.length()==0){
            return 0;

        }

        if(str.charAt(0)=='x'){
            counter ++;
        }

        return counter + countX(str.substring(1,(str.length())));
    }
}
