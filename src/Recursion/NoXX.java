package Recursion;

/**
 * Created by LalinPethiyagoda on 05/08/2017.
 */
public class NoXX {

    public static void main(String[] args) {
        System.out.println(noX("xaxb"));
    }

    public static String noX(String str) {
        if(str==null || str.length()==0){
            return "";
        }

       return  String.valueOf(str.charAt(0)=='x'?noX(str.replace("x","")): str.charAt(0) + noX(str.substring(1,str.length()))) ;

    }

}
