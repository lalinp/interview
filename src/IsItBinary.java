import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 01/11/2015.
 */
public class IsItBinary {
    public static void main(String[] args) {
        int number=0;
        int numLength=0;
        int nextNum=0;
        boolean result=true;
        Scanner sc = new Scanner(System.in);
        System.out.println("enter a number to check if its binary");

        number = sc.nextInt();
        numLength=number==0?1:(1+ (int)Math.floor(Math.log10(Math.abs(number))));
        for(int i=0;i<numLength-1;i++){
            nextNum=number%10;
            System.out.println(nextNum);
            result=nextNum>1?false:true;
            if(result=false) {
                System.out.println("not a binary");
                break;
            }
        }
        if(result){
            System.out.println("its a binary number");
        }
    }
}
