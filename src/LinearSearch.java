import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 28/10/2015.
 */
public class LinearSearch {
    int A[]={2,4,1,45,23,54,87};
    private boolean itemFound=false;

    public boolean linearSearchProgram(int searchValue){
        boolean result=false;
        for(int x=0;x<=A.length-1;x++){
            if(A[x]==searchValue){
                result=true;
                break;
            }
        }

        return result;

    }
    public static void main(String[] args){
        int searchValue=0;
        LinearSearch ls = new LinearSearch();
        Scanner sc = new Scanner(System.in);
        System.out.print("enter a number to search ");
        searchValue = sc.nextInt();
        System.out.println(ls.linearSearchProgram(searchValue));

        Integer A[]= new Integer[]{2, 4, 1, 45, 23, 54, 87};
        final int finalSearchVal=searchValue;
        boolean found= Arrays.stream(A).filter(x->x==finalSearchVal).findAny().isPresent();
        System.out.println(found);
    }
}
