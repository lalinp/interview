package dataStructures;

/**
 * Created by LalinPethiyagoda on 10/11/2015.
 */
public class Stack {
    private int maxSize;
    private long[] stackArray;//array variable - not instantiated yet
    private int top; // top of stack

    public Stack(int maxSize){
        this.maxSize=maxSize;
        this.top=-1;//nothing in the stack yet
        stackArray=new long[maxSize];//create the array
    }

    public void push(long j){
        String result = top==stackArray.length-1?" Stack is full": pushItem(j); // check if stack is full, if not call
        //pushItem(long j) method
        msgBox(result);
    }

    public String pushItem(long j){
        stackArray[++top]=j;
        return " Item added to stack";
    }

    public void pop(){
        msgBox(top==-1?" Stack is empty":popItem());
    }

    public String popItem(){
        // access item, THEN decrement top
        // once again,
        // behold the power of the post decrement! mwahaaaa!
        return "Item at the top : " + stackArray[top--]+ " removed";

    }
    //wrote this as I could not be bothered
    // with writing System.out.. yadi yadi yada everytime I wanted to display the output!
    public void msgBox(String message){
        System.out.println("** " + message + " **");
    }

    //check if stack is empty
    public boolean isEmpty(){
        return (top==-1);
    }

    //check if the stack is full
    public boolean isFull(){
        return (top==stackArray.length-1);
    }
}


