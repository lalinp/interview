package ExceptionHandling;

/**
 * Created by LalinPethiyagoda on 29/11/2015.
 * The code below does not do anything sensible,
 * but explains how the call stack works
 *
 * doItAll() calls, doItLevelOne() and
 * doItLevelOne() calls doItlevelTwo()
 */
public class ExceptionPropagation {

    public static void doItAll(){
        doItLevel1();
    }

    public static void doItLevel1(){
        doItLevel2();
    }

    public static void doItLevel2(){
        System.out.println("doint it at level 2");
    }

    public void processFiles(String fileName) throws Exception {


    }

    private  String loadFile(String s) {
        // configure the properties
        // this method can thow an exception, IOEXception, that will
        //not be managed from within the processFiles() method.
        //

        return "configuration complete";




    }

}
