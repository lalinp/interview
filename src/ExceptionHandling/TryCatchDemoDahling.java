package ExceptionHandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by LalinPethiyagoda on 19/11/2015.
 */
public class TryCatchDemoDahling {
    public  void FileReaderUtil() {

        try {
            File violinist = new File("LetticeRowBotham.txt");//new file object
            FileReader readLetticeFile = new FileReader(violinist); // code that might cause an exception

        } catch (FileNotFoundException fnfException) {//exception is caught here
            fnfException.printStackTrace(); // print the stack trace
        }
        finally{
            System.out.println("I  will be accessed and code inside me will always be executed!");
        }
    }

    public static void main(String[] args) {
        TryCatchDemoDahling tryCatchDemo= new TryCatchDemoDahling(); // create an intance of the class
        tryCatchDemo.FileReaderUtil();//invoke the method
        System.out.println();// random println dude!
    }
}
