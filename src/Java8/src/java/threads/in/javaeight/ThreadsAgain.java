package Java8.src.java.threads.in.javaeight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by LalinPethiyagoda on 15/07/2017.
 */
public class ThreadsAgain {
    static List<Integer> intList =  Collections.synchronizedList(new ArrayList());
    public static void main(String[] args) {
        final int value = 5;

        Runnable runRabbitOne = () -> even(5);
        Runnable runRabbitTwo = () -> odd();

        Thread one = new Thread(runRabbitOne, "t1");
        Thread two =  new Thread(runRabbitTwo, "t2");

        one.start();
        two.start();

        try{
            one.join();
            two.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

           /* for(Integer intl:intList){
                System.out.println("List content : " + intl);
            }*/

            //callable object
        Callable<Integer> callable = ()-> callableMethod();

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Future<Integer> result = executorService.submit(callable);

        while(!result.isDone()) {
            System.out.println("Ready yet? " + result.isDone());

        }

           try {
               System.out.println("callable result " + result.get());
           } catch (InterruptedException e) {
               e.printStackTrace();
           } catch (ExecutionException e) {
               e.printStackTrace();
           }

        executorService.shutdownNow();



        ExecutorService executorForCompletableFutures = Executors.newFixedThreadPool(10);
        Supplier<List<String>> emailList = () -> emailList();
        Function<String,Boolean> sendMessage =(email)->sendMessage(email);
        Consumer<Boolean> msg = (x)-> System.out.println(x);

        emailList().stream()
                .map(email->CompletableFuture.supplyAsync(()->email)
                .thenApply(sendMessage)
                .thenAccept(msg))
                .collect(Collectors.toList());


    }



    public static void even(int x){

        for(int i=0; i<=100;++i){
           if(i%2==0)
               intList.add(i*x);
        }


    }

    public static void odd(){

        for(int i=0; i<=100;++i){
           if(i%2==1)
               intList.add(i);
        }
    }

    public static int callableMethod(){int x = 0;
        for (int i = 0; i <= 1000; i++) {
            x = x + i * 5;
        }
        return x;}

        public static List<String> emailList(){
        List<String> emails = new ArrayList<>();
        emails.add("laleendra@gmail.com");
            emails.add("laleendra1@gmail.com");
            emails.add("laleendra2@gmail.com");
            emails.add("laleendra3@gmail.com");
            emails.add("laleendra4@gmail.com");
            emails.add("laleendra5@gmail.com");
            emails.add("laleendra6@gmail.com");
            return emails;

        }

        public static boolean sendMessage(String email){
            System.out.println("sending " + email);
            return true;
        }
}
