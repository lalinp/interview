package Java8.src.java.threads.in.javaeight;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

/**
 * Created by LalinPethiyagoda on 29/05/2017.
 */
@ThreadSafe
public class CurrentAccount {
@GuardedBy("this")
    private double balance=1000;

    public CurrentAccount(){}

    public  void deposit(double amount){
        synchronized (this) {
            balance = balance + amount;
        }
    }

    public synchronized void withdraw(double amount) throws InsufficientFundsException{

        if((amount-balance<0)){
            throw new InsufficientFundsException("insufficient funds ");
        }

    }
    public synchronized double getBalance(){
        return this.balance;
    }
}
