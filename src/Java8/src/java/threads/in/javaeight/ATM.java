package Java8.src.java.threads.in.javaeight;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by LalinPethiyagoda on 29/05/2017.
 */
public class ATM {

    public static void main(String[] args) {

        CurrentAccount ca = new CurrentAccount();
        ExecutorService pool = Executors.newFixedThreadPool(10);
        final double depositAmount =100;
        final double withdrawalAmount = 55;

        Runnable updateOne = () ->  ca.deposit(depositAmount);
        Runnable updateTwo = () ->  ca.deposit(2000);
        Thread tOne = new Thread(updateOne);
        Thread tTwo = new Thread(updateTwo);
        tOne.start();
        tTwo.start();


                System.out.println(ca.getBalance());
    }
}
