package Java8.src.java.threads.in.javaeight;

import java.util.concurrent.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created by LalinPethiyagoda on 13/05/2017.
 */
public class LearningThreads {

    //use Runnable functional interface to define a runnable task


    public static void main(String[] args) {
        Runnable runnableTask = () -> {
            System.out.println("inside run");
            System.out.println(Thread.currentThread().getName());

        };

       /* runnableTask.run();

        Thread thread = new Thread(runnableTask);

        thread.start();*/
        System.out.println("done");
        // the execution of threds is non-determinisic

        ExecutorService executorService = Executors.newSingleThreadExecutor(); // one thread
        executorService.submit(runnableTask);

        // using callable

        Callable<Long> callableObj = () ->{
            long sum = 0;
            for (long i = 0; i <= 10; i++) {
                sum += i;
            }
            return sum;
        };

        ExecutorService callableExecutor = Executors.newSingleThreadExecutor();
        Future<Long> captureFutureObjectReturnedBySubmit = callableExecutor.submit(callableObj);
        try {
            long x = captureFutureObjectReturnedBySubmit.get();
            System.out.println("total " + x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Supplier<Long> longSupplier = ()->x();
        Consumer<Long> notify=(Long x)-> System.out.println("this is " + x.toString());
        /*Consumer Represents an operation that accepts a single input argument and returns no
 * result.*/

       /* new CompletableFuture<Long>().runAsync(stringSupplier,callableExecutor);*/
        CompletableFuture.supplyAsync(longSupplier,callableExecutor)
                .thenAccept(notify);

        int numCores = Runtime.getRuntime().availableProcessors();
        System.out.println("cores " + numCores);

        ExecutorService pool = Executors.newFixedThreadPool(10);

        Supplier<String> longRuningTask = ()-> "50";
        CompletableFuture<String> stringCompletableFuture = new CompletableFuture<>();
        CompletableFuture.supplyAsync(longRuningTask,pool)
        .thenApply(Integer::parseInt)
        .thenApply(r->r*r*Math.PI);

        final int a = 3;
        final int b = 5;
        LearningThreads l = new LearningThreads();
        Supplier<Integer> sumOfNumbers= () -> l.addTwo(a,b);
        Function<Integer,Integer> timesTwo =(c)->{
            System.out.println("multiplying by two");
            return c*2;
        };
        Function<Integer,Integer> timesThree =(c)->{
            System.out.println("multiplying by three");
            return c*3;
        };

        CompletableFuture.supplyAsync(sumOfNumbers,pool)
                .thenApply(timesTwo)
                .thenApply(timesThree)
                .thenAcceptAsync(result ->System.out.println(result));

        CompletableFuture<String> future = CompletableFuture.completedFuture("foo");






    }

    static Long x(){
        return new Long(5);
    }

    public synchronized  int  addTwo(int a, int b){
        System.out.println("addition done");
        return a + b;
    }

}
