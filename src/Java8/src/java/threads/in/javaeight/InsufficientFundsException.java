package Java8.src.java.threads.in.javaeight;

/**
 * Created by LalinPethiyagoda on 29/05/2017.
 */
public class InsufficientFundsException extends Exception {
    String message;


    public InsufficientFundsException(){
        super();

    }
    public InsufficientFundsException(String message){
        super(message);

    }
    public InsufficientFundsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientFundsException(Throwable cause) {
        super(cause);
    }
}
