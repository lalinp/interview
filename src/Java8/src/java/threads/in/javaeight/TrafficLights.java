package Java8.src.java.threads.in.javaeight;

import java.util.concurrent.*;

/**
 * Created by LalinPethiyagoda on 15/08/2017.
 */
public class TrafficLights {
    static Runnable NorthToSouthRoad = () -> {
        System.out.println("Northsouth-Green");
        System.out.println("EastWest-Red");
    };

    static Runnable EastToWest = () -> {
        System.out.println("Northsouth-RED");
        System.out.println("EastWest-GREEN");
    };

    static Callable<Boolean> lightUpGreen = () ->{return true;};
    static Callable<Boolean> lightUpAmber = () ->{return true;};
    static Callable<Boolean> lightUpRed = () ->{return true;};

    public static void main(String[] args) {
        boolean isGreenLit=false;
        ScheduledExecutorService timer = Executors.newScheduledThreadPool(2);
        timer.scheduleAtFixedRate(NorthToSouthRoad,10,19, TimeUnit.SECONDS);
        timer.scheduleAtFixedRate(EastToWest,20,19, TimeUnit.SECONDS);

        ScheduledFuture<Boolean> resultGreenL4it  = timer.schedule(lightUpGreen,10,TimeUnit.SECONDS);

    }



}
