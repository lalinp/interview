package Java8.src.java.eight.streams;

/**
 * Created by LalinPethiyagoda on 12/05/2017.
 */
public enum Topic {
    HISTORY,
    PROGRAMMING,
    MEDICINE,
    COMPUTING,
    FICTION

}
