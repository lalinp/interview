package Java8.src.java.eight.streams;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static Java8.src.java.eight.streams.Topic.*;

/**
 * Created by LalinPethiyagoda on 12/05/2017.
 */
public class StreamRunner {
    static List<Book> bookList;

    public static void main(String[] args) {
        bookList = generateRepository();

        bookList.stream()
                .filter(book -> book.getPubDate().isAfter(Year.of(2002)))
                .sorted(Comparator.comparing(Book::getTitle))
                .forEach(book -> System.out.println(book.getTitle()));

        Arrays.asList(1,3,0,1,3,5,6,8,5,8).stream()
                .sorted(Integer::compareTo)
                .forEach(System.out::println);

        /* A stream that contans only computing books */
        bookList.stream()
                .filter(book -> book.getTopic() == Topic.COMPUTING)
                .map(book -> book.getTitle())
                .forEach(System.out::println);
        System.out.println("****************** A stream of books, sorted by title *****************");
         /* A stream of books, sorted by title */
         bookList.stream()
                 .sorted(Comparator.comparing(Book::getTitle))
                 .map(book -> book.getTitle())
                 .forEach(System.out::println);

         /* STREAM OF AUTHORS in order of book titles and duplicate authors removed
          */

        System.out.println("****************** STREAM OF AUTHORS in order of book titles and duplicate authors removed *****************");


        bookList.stream()
                .sorted(Comparator.comparing(Book::getTitle))
                .flatMap(book -> book.getAuthors().stream())
                .distinct()
                .forEach(System.out::println);


        List<String> list = new ArrayList<String>();
        list.add("java");
        list.add("php");
        list.add("python");
        list.add("perl");
        list.add("c");
        list.add("lisp");
        list.add("c#");

        int num = list.stream()
                .mapToInt(x->x.length())
                .sum();

        int reduce = list.stream().
                map(x->x.length())
                .reduce(0,(x, y)->x+y);

        System.out.println("num :" + num);
        System.out.println("reduce :" + reduce);
    }


    public static List<Book> generateRepository() {
        bookList = new ArrayList<>();
        Book roots = new Book("Roots", Arrays.asList("Lalin", "Prasadi", "Georgia"), new int[]{234, 512}, HISTORY, Year.of(2000), 234.5);
        Book concurrency = new Book("Java Concurrency In Practice", Arrays.asList("Josh Bloch", "Lalin", "Bently"), new int[]{234, 300, 400}, COMPUTING, Year.of(2000), 123.5);
        Book pearls = new Book("Programming Pearls", Arrays.asList("Bently"), new int[]{234, 512}, COMPUTING, Year.of(2002), 456.5);
        Book kiteRunner = new Book("The Kite Runner", Arrays.asList("Hossaini"), new int[]{123, 672}, FICTION, Year.of(2010), 236.5);
        Book happiness = new Book("How to be happy", Arrays.asList("Dalai Lama"), new int[]{456}, MEDICINE, Year.of(2017), 356.5);

        bookList.add(roots);
        bookList.add(concurrency);
        bookList.add(pearls);
        bookList.add(kiteRunner);
        bookList.add(happiness);

        return bookList;
    }



}


