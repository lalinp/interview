package Java8.src.java.eight.streams;

import java.time.Year;
import java.util.List;

/**
 * Created by LalinPethiyagoda on 12/05/2017.
 */
public class Book {
    private String title;
    private List<String> authors;
    private int[] pageCounts;
    private Topic topic;
    private Year pubDate;
    private double height;

    public Book(String title, List<String> authors, int[] pageCounts, Topic topic, Year pubDate, double height){
        this.title= title;
        this.authors=authors;
        this.pageCounts=pageCounts;
        this.topic = topic;
        this.pubDate = pubDate;
        this.height = height;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public int[] getPageCounts() {
        return pageCounts;
    }

    public Topic getTopic() {
        return topic;
    }

    public Year getPubDate() {
        return pubDate;
    }

    public double getHeight() {
        return height;
    }
}

