package Java8.src;

import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

/**
 * Created by LalinPethiyagoda on 18/04/2017.
 */


public class Book {

    private String title;
    private String authorFName;
    private String authorLName;
    private int pages =0;

    public Book(String title, String fname, String lname, int pages){

        this.title=title;
        this.authorFName=fname;
        this.authorLName = lname;
        this.pages = pages;
    }

    public synchronized String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorFName() {
        return authorFName;
    }

    public void setAuthorFName(String authorFName) {
        this.authorFName = authorFName;
    }

    public String getAuthorLName() {
        return authorLName;
    }

    public void setAuthorLName(String authorLName) {
        this.authorLName = authorLName;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }
}
