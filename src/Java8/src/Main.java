package Java8.src;

import java.util.function.*;

public class Main {


    public static void main(String[] args) {
        //predicate functional interface uses test() method to execute the function
        Predicate<String> stringPredicate = (s)-> s.length() < 10;
        System.out.println(stringPredicate.test("lalin"));

        //consumer : uses accept() to execute the instruction
        //Consumer consumes= does not return a value

        Consumer<String> consumerFunction = (s)-> System.out.println(s.length()); // define the function This is the function body
        //now to use
        consumerFunction.accept("Lalin");

        //Function functional interface: this y=uses the apply() method to execute
        Function<String, Integer> function=(s)->{
            return s.length(); };
        System.out.println(function.apply("hello"));

        //Supplier
        // Does not take an arg - returns a a value of a given type
        // uses get() the value of the type
        Supplier<String> supplierFunction = () -> "Hello there";

        System.out.println(supplierFunction.get());

        Supplier<Integer> intSupplier = () -> 5;
        System.out.println(intSupplier.get());

        Function<String,Integer> funcStr = (s) -> s.length();

        Supplier<Integer> supInt = () -> funcStr.apply("hello0000");
        System.out.println(supInt.get());

        BinaryOperator<Integer> addTwo = (a,b) -> a+b;
        System.out.println(addTwo.apply(5,10));

        myGreetingFunction<String> message = (s) -> System.out.println(s);
       // System.out.println(message.sayHello("hey"));

        CalculateValues add = (a,b) -> a + b;
        CalculateValues sub = (a ,b) -> a - b;
        CalculateValues mult = (a ,b) -> a * b;

        System.out.println(add.calc(1,2));
        System.out.println(mult.calc(5,2));
        System.out.println(sub.calc(10,2));

        testMethod(1,2,message);

    }
    @FunctionalInterface
    interface myGreetingFunction<T>{
        void sayHello(T s);
    }
    @FunctionalInterface
    interface  CalculateValues{
        int calc(int a, int b);
    }

    public static void testMethod(int x, int y, myGreetingFunction<String> greetme){
        //System.out.println("using a function type in the method sig");
        int z = x + y;
        greetme.sayHello("inside testMethod " + String.valueOf(z) );
    }

}
