import java.util.function.Function;

/**
 * Created by LalinPethiyagoda on 18/04/2017.
 */
public class DoingThreads {
    public static void main(String[] args) {
        int x =5;
        int y = 6;
         int nOqne;

        Runnable runRabbitOne = () -> System.out.println("rabbitOne");
        Thread runRabbit = new Thread(runRabbitOne);
        runRabbit.start();

        //define the method body for calc for each case
        //three references to calc here
        Calculator add = (a,b)->a+b;
        Calculator sub = (a,b)->a-b;
        Calculator mult = (a,b)->a*b;
        Function<Calculator,Integer>p= (s)->s.calc(x,y);


        final int[] addTwo = new int[1];
        final int[] multTwo = new int[1];
        final int[] subTwo=new int[1];
        Thread t1 = new Thread(() -> {
            System.out.println("t1");
            p.apply(add); // classic case of mutating local variables using a lambda
        });
        Thread t2= new Thread(() -> {
            System.out.println("t2");
            multTwo[0] =sub.calc(5,3);
        });

        Thread t3= new Thread(() -> {
            System.out.println("t3");
            subTwo[0] =mult.calc(5,3);
        });
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        int tot = addTwo[0] + multTwo[0] + subTwo[0];
        System.out.println(tot);

    }

    @FunctionalInterface
    interface Calculator{
        public int calc(int a,int b);
    }
}
