package Java8.src;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Comparator.comparing;

/**
 * Created by LalinPethiyagoda on 18/04/2017.
 */
public class JavaCollections {
    /*
    Set - No duplicates
    List - an Orderd Collection
    Map - maps keys to values

     */
    public static void main(String[] args) {
        List<String> names = Arrays.asList("Paul","John","Ringo","Adam","Peter");

        names.forEach(System.out::println);
        names.forEach(System.out::println);
        Collections.sort(names, new Comparator<String>(){
            @Override
            public int compare(String a, String b){
               return a.compareTo(b);
            }
                }
        );
names.forEach(System.out::println);

        System.out.println("----");
        /*System.out.println(names);

        // now using lambdas now in descending order
        List<String> namesForLambdas = Arrays.asList("Paul","John","Ringo","Adam","Peter");
        Comparator<String> comp = (a,b)->b.compareTo(a);
        Collections.sort(namesForLambdas,comp);
        System.out.println(namesForLambdas);*/

        //Books
        Java8.src.Book book1 = new Book("Miss Peregrines Home for Peculiar Children", "Ranson", "Riggs", 382);
        Book book2 = new Book("Harry Potter and the Sorcerers Stone","JK" , "Rowling", 411);
        Book book3 = new Book("The Cat in the Hat","Dr","Seuss",45);

        List<Book> books = Arrays.asList(book1,book2,book3);
        // add up the pages in each book

        Comparator<Book> compareByPageNumber =comparing(book->book.getPages());
        books.stream()
                .sorted(compareByPageNumber)
                .map(Book::getTitle)
                .collect(Collectors.toList())
                .forEach(nams ->System.out.println("*** " + nams));


        int total = books.stream().collect(Collectors.summingInt(Book::getPages));

        //Stream mapToInt(ToIntFunction<? super T> mapper) returns an IntStream by applying the given function to this stream.
        System.out.println(books.stream().mapToInt(Book::getPages).sum());

        List<String> numsList = Arrays.asList("1","2","3","4","5","6","7","8");
        numsList.stream().mapToInt(numStr->Integer.parseInt(numStr)).filter(num->num%2==0).forEach(System.out::println);
        Consumer<String> consumerFunction = (s)-> System.out.println(s); // define the function This is the function body
        //now to use

        //accumulate the authors last name to a list
        List<String>lastNamesList = books.stream().map(Java8.src.Book::getAuthorLName).collect(Collectors.toList());
        books.stream().map(Java8.src.Book::getAuthorLName).forEach(str ->consumerFunction.accept(str));
        books.stream().map(Book::getAuthorFName).forEach(consumerFunction);
        books.stream().map(Book::getTitle).forEach(consumerFunction);

        Arrays.asList("red","blue","green")
                .stream()
                .sorted()
                .findFirst()
                .ifPresent(System.out::println);

        Arrays.asList("apples","pear","banana","cherry","apricot","avacoado")
                .stream()
                .filter(fruit->fruit.startsWith("a"))
                .forEach(System.out::println);


        Integer[] integerArray = new Integer[]{1,4,5,3,2,12};

        //using a specific comparator to sort
        Arrays.asList(integerArray);
        Arrays.stream(integerArray).forEach(System.out::print);
        Arrays.sort(integerArray,Integer::compareUnsigned);
        Arrays.stream(integerArray).forEach(num -> System.out.print(num + " ,"));

        OptionalInt max= IntStream.rangeClosed(1,100).parallel().map(num-> num*2).max();
        System.out.println(max);
    }


}
