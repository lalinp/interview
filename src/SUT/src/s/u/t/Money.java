package s.u.t;

/**
 * Created by LalinPethiyagoda on 04/04/2017.
 */
public class Money {
    private final int amount;
    private final String currency;
    private  double totalTax;

    public Money(int amount, String currency){
        if(amount<0) throw new IllegalArgumentException("amount cannot be less than zero [" + amount + "]");

        if(currency == null || currency.isEmpty()){
            throw new IllegalArgumentException("Currency must consist of a valid value ");
        }
        this.amount = amount;
        this.currency = currency;

    }

    public int getAmount(){
        return amount;
    }

    public String getCurrency(){
        return currency;
    }

    public boolean equals(Object object){
        if(object instanceof Money){
            Money moneyObj = (Money)object;
            return moneyObj.getCurrency().equals(this.getCurrency()) &&
                    moneyObj.getAmount()==this.getAmount();
        }
        return false;
    }

    public double calculateTax(){
        return getAmount()*0.25;
    }
    public void setTotalTax(){
        this.totalTax =calculateTax();
    }
    public double getTotalTax(){
        setTotalTax();
        return totalTax;
    }
}
