package s.u.t;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by LalinPethiyagoda on 05/04/2017.
 */
public class HashMapClass {
    public Map getHashMap() {
        return hashMap;
    }

    public void setHashMap(Map hashMap) {
        this.hashMap = hashMap;
    }

    private Map hashMap = new HashMap<Integer,String>();

    public void addAnObject(int key, String str){
       if(hashMap.containsKey(key)){
           hashMap.replace(key,hashMap.get(key),str);
           System.out.println("updated " + key + " " + str);
       }else{
            hashMap.put(key,str);
            System.out.println("added " + key + " " + str);}

    }

    public String getAnObject(int key){
        return (String)hashMap.get(key);
    }

    public void clear(){
        hashMap.clear();
    }

}
