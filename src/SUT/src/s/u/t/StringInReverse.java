package s.u.t;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LalinPethiyagoda on 05/04/2017.
 */
public class StringInReverse {
    static String stringToReverse;
    public String getStringToReverse() {
        return stringToReverse;
    }

    public StringInReverse(String str){
        if(str==null || str.isEmpty()){
            throw new IllegalArgumentException("invalid data submitted");
        }
        this.stringToReverse = str;
    }

    public void setStringToReverse(String stringToReverse) {
        if(stringToReverse==null || stringToReverse.isEmpty()){
            throw new IllegalArgumentException("invalid data submitted");
        }
        this.stringToReverse = stringToReverse;
    }



    public static String reverseString(){
        List<String> tempArray = new ArrayList<>(stringToReverse.length());
        for(int i=0; i<stringToReverse.length();i++){
            tempArray.add(stringToReverse.substring(i,i+1));
        }

        StringBuilder inReverse = new StringBuilder(stringToReverse.length());
        for(int i=tempArray.size()-1;i>=0;--i){
            inReverse.append(tempArray.get(i));
        }
        return inReverse.toString();
    }
}
