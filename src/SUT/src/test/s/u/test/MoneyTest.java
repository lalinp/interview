package test.s.u.test;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import s.u.t.Money;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by LalinPethiyagoda on 04/04/2017.
 */

@RunWith(JUnitParamsRunner.class)
public class MoneyTest {
    private static final int VALID_AMOUNT =5;
    private static final String VALID_CURRENCY = "GBP";
    private static final Object[] getMoney(){
        return new Object[]{new Object[]{100,"USD",25.00},
                            new Object[]{200,"EUR",50.00}};
    }
    @Test
    @Parameters(method="getMoney")
    public void Constructor_Should_Set_Amount_And_Currency_Test(int amount, String currency,double totalTax){
        final double DELTA = 1e-15;
        /*
            *delta - the maximum delta between expected and actual
            *for which both numbers are still considered equal.
         */
        Money money = new Money(amount, currency);
        assertEquals(amount, money.getAmount());
        assertEquals(currency,money.getCurrency());//expected , actual
        assertNotEquals("LKR", money.getCurrency());
        assertTrue(money.getAmount() == amount);
        assertEquals(totalTax,money.getTotalTax(),DELTA);
        assertThat(totalTax, equalTo(money.getTotalTax())); // No DELTA
    }


   public final Object[]getInvalidAmount(){
       return new Integer[][]{{-12345},{-5},{-1}};
   }
    @Test(expected = IllegalArgumentException.class)
    @Parameters(method="getInvalidAmount")
    public void ConstructorThrowsExceptionWhenInvalidAmountEntered(int amount){
        new Money(amount, VALID_CURRENCY);
    }

    public final Object[] getInvalidCurrency(){
        return new String[][]{{null},{""}};
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters(method ="getInvalidCurrency")
    public void ConstructorThrowsExceptionWhenInvalidCurrencyEntered(String currency){
        new Money(VALID_AMOUNT, currency);
    }
}
