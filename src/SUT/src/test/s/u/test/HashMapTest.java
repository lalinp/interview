package test.s.u.test;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import s.u.t.HashMapClass;

import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 * Created by LalinPethiyagoda on 05/04/2017.
 */
@RunWith(JUnitParamsRunner.class)
public class HashMapTest {
    Map mapOneStub;
    HashMapClass hmc;
    private static final int KEY_VAL =1;
    private static final String UPDATE_STRING ="HELLO WORLD";

    @Before
    public void setUp(){
        mapOneStub=new HashMap<Integer,String>();
        mapOneStub.put(1,"Lalin");
        mapOneStub.put(2,"ALICE");
        mapOneStub.put(3,"IN WONDERLAND");
        hmc = new HashMapClass();
    }
    @Test
    public void canInstantiateHashMap(){
        hmc.setHashMap(mapOneStub);
        assertEquals(mapOneStub, hmc.getHashMap());
    }

    private static final Object[] getKeyValuePairs(){
        return new Object[]{new Object[]{4,"HELLO"},
                new Object[]{2,"ALICE"},
                new Object[]{3,"IN WONDERLAND"}};
    }
    @Test
    @Parameters(method = "getKeyValuePairs")
    public void addAnItemToTheMap(int key, String value){
        hmc.addAnObject(key, value);
        assertEquals(value,hmc.getAnObject(key));
    }
    @Test
    public void updateItemsInMap(){
        hmc.setHashMap(mapOneStub);
        hmc.addAnObject(1,UPDATE_STRING);
        hmc.addAnObject(2,UPDATE_STRING);
        assertEquals(UPDATE_STRING,hmc.getAnObject(1));
    }

}
