package test.s.u.test;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import s.u.t.StringInReverse;

import static org.junit.Assert.assertEquals;

/**
 * Created by LalinPethiyagoda on 05/04/2017.
 */
@RunWith(JUnitParamsRunner.class)
public class StringInReverseTest {
    private static final String IN_REV ="dlrow olleH";
    private static final Object[] getStrings(){
        return new Object[]{new Object[]{"Hello world"},
                new Object[]{"Lanka Rocks"}};
    }

    @Test
    @Parameters(method="getStrings")
    public void constructorShouldSetStringToReverse(String str){
        StringInReverse sir = new StringInReverse(str);
        assertEquals(str, sir.getStringToReverse());
    }

    public static final Object[] getInvalidString(){
        return new  String[][]{{null},{""}};
    }
    @Test(expected = IllegalArgumentException.class)
    @Parameters(method ="getInvalidString")
    public void constructorHasReceivedAnInvalidString(String str){
        new StringInReverse(str);
    }

    @Test
    public void accuratelyReversesText() {
        assertEquals(IN_REV, new StringInReverse("Hello world").reverseString());
    }
}
