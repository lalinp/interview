package Sorting;

/**
 * Created by LalinPethiyagoda on 04/11/2015.
 */
public class BubbleSort {
    public static void main(String[] args) {
        int[] a ={22,5,56,49,2};//array of five digits
        int tempVal=0;
        int currVal=0;
        int nextVal =0;

        //the outer for loop manages the passes (4 in this case)
        for(int i=0;i<a.length-1;++i){
            for(int j=0;j<a.length-1;++j){
                if(a[j]>a[j+1]){
                    tempVal = a[j];
                    a[j]=a[j+1];
                    a[j+1]=tempVal;

                }
            }
        }

        //lets print the sorted array
        for(int i=0; i<a.length; ++i){
            System.out.print((a[i] + " "));
        }

    }
}
