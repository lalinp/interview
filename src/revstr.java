import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 24/07/2017.
 */
public class revstr {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter a string");
        String str = scanner.nextLine();

        String reversed = reversingAString(str);
        System.out.println(reversed);
    }

    public static String reversingAString(String str){
        String inRev="";
        if(str==null){
            return null;
        }
        else if(str.length()==1){
            return str;
        }

        inRev = inRev + str.charAt(str.length()-1) + reversingAString(str.substring(0,str.length()-1));

                return inRev;
    }
}
