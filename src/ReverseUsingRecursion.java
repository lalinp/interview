import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 30/01/2016.
 * rever a string using recursion
 */
public class ReverseUsingRecursion {

    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        System.out.println("enter a string ");
        String str = sc.nextLine();

        String rev = revString(str);
        System.out.println(rev);
    }

    public static String revString(String str){
        String rev="";
        if(str.length()==0){
            return null;//one char
        }
        else if(str.length()==1){
            return str;
        }
        else{
            rev = rev + str.charAt(str.length()-1)+revString(str.substring(0,str.length()-1));
            return rev;
        }



    }

}
