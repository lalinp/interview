package ComparabeExample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by LalinPethiyagoda on 02/01/2016.
 */
public class StudentController {
    public static void main(String[] args) {
        Student s1 = new Student(1, "Jam", 53);
        Student s2 = new Student(2, "Fruit", 54);
        Student s3 = new Student(3, "Tree", 56);
        Student s4= new Student(4, "Sonna Boy Von Bloss", 87);
        Student s5= new Student(5,"CarloBoy Von Bloss", 2);

        List<Student> studentList = new ArrayList<Student>();
        studentList.add(s1);
        studentList.add(s2);
        studentList.add(s3);
        studentList.add(s4);
        studentList.add(s5);

        Collections.sort(studentList, new MarksComparator());

        for(Student s: studentList){
            System.out.println(s.getName());
        }

        /**
        System.out.println(s1.compareTo(s2));
        int result = s1.compareTo(s2);
        //res can have three values 1, 0 -1

         *  1: (a positive number) this particular object is greater than the one that we are comparing with
         *  0: This object is the same as the one that we are comparing with
         * -1: (Any negative number) This particular object is less than the one that we are comparing with


        if(result<0){
            System.out.println(s2.getName() + " comes before " + s1.getName());
        }

        if(result>0){
            System.out.println(s1.getName() + " comes before " + s2.getName());
        }
        else{
            System.out.println(s1.getName() + " == " + s2.getName());
        }
         */
    }
 }