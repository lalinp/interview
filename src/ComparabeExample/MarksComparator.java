package ComparabeExample;

import java.util.Comparator;

/**
 * Created by LalinPethiyagoda on 03/01/2016.
 */
public class MarksComparator implements Comparator<Student> {
    @Override
    public int compare(Student s1, Student s2) {

        //ascending order
        //return (s1.getMarks()-s2.getMarks());

        return s2.getMarks()-s1.getMarks();
    }
}
