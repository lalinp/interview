package ComparabeExample;

/**
 * Created by LalinPethiyagoda on 02/01/2016.
 *
 * Comparable<T> by providing what to compare
 * in the angle brackets allow the elimination of
 * type casting and compareTo(Object o) can be replaced with
 * int compareTo(The Specific Object type variable)
 * for example:
 * int compareTo(Student stud)
 */
public class Student implements Comparable<Student> {

    private int id=0;
    private String name;
    private int marks=0;

    public Student(int sid, String studName,int studMarks){
        this.id= sid;
        this.name=studName;
        this.marks=studMarks;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public int compareTo(Student student){
        //int val=0;

        return Integer.valueOf(this.getMarks()).compareTo(student.getMarks());
       /**

        if(getMarks()>student.getMarks()){
            val= 1;
        }

        else if(getMarks()==student.getMarks()){
            val= 0;
        }
        else if(getMarks()<student.getMarks()){
            val= -1;
        }

        else if(getName().compareTo(student.getName())>0){
            val =1;
        }

        else if(getName().compareTo(student.getName())<0){
            val=-1;
        }
        else if(getName().compareTo(student.getName())==0){
            val =0;
        }
        return val;
        */
    }
}
