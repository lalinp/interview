/**
 * Created by LalinPethiyagoda on 23/10/2015.
 * Strings in Java have four regex methods
 * matches()
 * split()
 * replaceFirst()
 * replaceAll()
 * replace() is not supported  by String
 */
public class RegexExample {

    public static void main(String[] args){
        String s="lallin is the coolest guy";
        System.out.println(s.matches("lallin"));
    }
}
