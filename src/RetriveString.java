import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by LalinPethiyagoda on 22/10/2015.
 */
public class RetriveString {

    public static void main(String[]args){
        String strToSearch = "<p>Coders' favourite beverages:</p><ol class=\"favourites\">\n" +
                "               <li class=\"item\">Coffee</li>\n" +
                "               <li class=\"item\">Tea</li>\n" +
                "               <li class=\"item\">Cola</li>\n" +
                "</ol>";
        Pattern pattern = Pattern.compile("<li>(\\S+)</li>");
        Matcher matcher = pattern.matcher(strToSearch);
        if (matcher.find())
        {
            // get the matching group
            String ol_Li_Beverages_group = matcher.group(1);

            // print the group
            System.out.format("'%s'\n", ol_Li_Beverages_group);
        }
        }
    }

