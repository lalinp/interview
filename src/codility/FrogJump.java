package codility;

/**
 * Created by LalinPethiyagoda on 10/10/2016.
 */
public class FrogJump {
    public int solution(int start, int destination, int jump) throws Exception {
        int jumpCounts =0;
        if (destination < start || jump<0 ){
            throw new Exception("Invalid arguments");
        }
        if((destination-start)%2==0){
            jumpCounts= (destination-start)/jump;
        }
        else{
            jumpCounts= (destination-start)/jump + 1;
        }
        return jumpCounts;
    }

    public static void main(String[] args) throws Exception {
        FrogJump fj = new FrogJump();
        System.out.println(fj.solution(10,85,30));
    }


}
