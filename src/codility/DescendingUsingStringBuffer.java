package codility;

/**
 * Created by LalinPethiyagoda on 11/10/2016.
 */
public class DescendingUsingStringBuffer {
    public static int sortDesc(final int num) {
       String str = String.valueOf(num);
        String sbRev=new StringBuilder(str).reverse().toString();

        return Integer.parseInt(sbRev);
    }

    public static void main(String[] args){
        System.out.println(sortDesc(987654321));
    }
}
