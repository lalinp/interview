package codility;

/**
 * Created by LalinPethiyagoda on 13/10/2016.
 */
public class ImprovedBinaryGap {
    public static int solution(int N){
        String binaryStr = Integer.toBinaryString(N);
        //System.out.println(binaryStr);
        String partsStr=binaryStr.substring(binaryStr.indexOf('1'),binaryStr.lastIndexOf('1')+1);
        //System.out.println(partsStr);
        String[] parts = partsStr.split("1");
        int currLength=0;
        int result=0;
        for(int i=0; i < parts.length; i++){
            currLength=parts[i].length();
            //System.out.println(parts[i]);
            result=currLength>result?currLength:result;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(solution(51712));
    }

}
