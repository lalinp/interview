package codility;



import java.util.Arrays;

/**
 * Created by LalinPethiyagoda on 17/10/2016.
 */
public class OddOccur {
    public static int solution(int[]intArray){
        Arrays.sort(intArray);
       int result=0;
        for(int i=0;i<=intArray.length-1;++i){
            System.out.println("before " +result);
            System.out.println("result " + result + " XOR " + intArray[i] );
            result=result^intArray[i];
            System.out.println("after " +result);
        }

        return result;
    }

    public static void main(String[] args) {
        int[]myArray = new int[]{9,3,9,3,1,6,6};
        int x = solution(myArray);
        System.out.println(x);
    }
}
