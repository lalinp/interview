package codility;

import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 02/02/2016.
 */
public class BinaryGap {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String binaryNumber = sc.next();
        //int[] gapArray={};
        int count = 0;
        String gaps="";
        int arrayIndex =0;
        int j = binaryNumber.indexOf("1");
        for(int k=0; k<binaryNumber.length()-1;k++) {
            if (binaryNumber.charAt(k) == '1') {
                for (int i = k + 1; i <= binaryNumber.indexOf('1',k+1); i++) {
                    if (binaryNumber.charAt(i) == '0') {
                        //System.out.println(" ** " + binaryNumber.charAt(i));
                        count++;
                    }
                }
                gaps += count + " , ";
                count=0;
            }

        }
        System.out.println(gaps);
    }
}
