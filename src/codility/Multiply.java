package codility;

/**
 * Created by LalinPethiyagoda on 10/10/2016.
 */
public class Multiply {
    public static Double multiply(Double a, Double b) {
        return a * b;
    }
}
