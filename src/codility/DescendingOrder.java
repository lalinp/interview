package codility;

/**
 * Created by LalinPethiyagoda on 11/10/2016.
 */
public class DescendingOrder {
    public static int sortDesc(final int num) {
        String numAsString = String.valueOf(num);
        char[] charArray = numAsString.toCharArray();
        String inRev=numAsString.substring(numAsString.length()-1,numAsString.length()-1+1);
        for(int i=numAsString.length()-2; i>=0; i--){
           inRev = inRev + numAsString.substring(i,i+1);
        }
        return Integer.parseInt(inRev);
    }

    public static void main(String[] args){
        System.out.println(sortDesc(2110));
    }
}
