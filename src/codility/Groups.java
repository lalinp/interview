package codility;

/**
 * Created by LalinPethiyagoda on 11/10/2016.
 */
public class Groups {
    public static boolean groupCheck(String s){
        boolean result=false;
        if (s==null|| s==""){
            return result;
        }
        String inRev = new StringBuffer(s).reverse().toString();
        char[] cRev = inRev.toCharArray();
        char[] sArr = s.toCharArray();
        System.out.println(sArr);
        System.out.println(cRev);
        for(int i=0; i < sArr.length-1; i++){
            if(sArr[i]=='('){
                if (cRev[i]==')')
                {result=true;} else{result=false; break;}}
            if(sArr[i]==')'){
                if (cRev[i]=='(')
                {result=true;} else{result=false; break;}}
            else if(sArr[i]=='['){
                if (cRev[i]==']'){result=true;}else{result=false;  break;}
            }
            else if(sArr[i]==']'){
                if (cRev[i]=='['){result=true;}else{result=false;  break;}
            }
            else if(sArr[i]=='{'){
                if (cRev[i]=='}'){result=true;}else{result=false; break;}
            }
            else if(sArr[i]=='}'){
                if (cRev[i]=='{'){result=true;}else{result=false; break;}
            }

        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(groupCheck("({})"));
    }
}
