import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 01/11/2015.
 * definition of Floyd's triangle : "It's a right angled triangle of array of natural numbers, which is named after Robert Floyd. It is defined by filling the rows of the triangle with consecutive numbers, stating with 1 in the top left corner".
 * It looks like following pattern :
 * 1
 * 2  3
 * 4  5  6
 * 7  8  9  10
 * 11  12  13  14  15
 *
 * Your task is to print this table using nested loops in Java.
 */
public class FloydsTriangle {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfRows=0;
        int number =1;
        System.out.print("enter number of rows in the Floyed's triangle to display ");
        numberOfRows=sc.nextInt();

        for(int i=1;i<=numberOfRows;i++){
           // System.out.print(i + " ");
            for(int j=1;j<=i;j++){
                System.out.print(number + " ");
                number++;
            }
            System.out.println();

        }
    }
}
