package Searching;

/**
 * Created by LalinPethiyagoda on 28/10/2015.
 */
public class BinarySearch {
    static int[] a = {1 ,2,12,22,33,100};
    static int x = 22;

    public static void main(String[] args) {
        String result = binarySearch(a,x)?"value found" :"Not found";
        System.out.println(result);
    }
    public static boolean binarySearch(int[] array, int value)  {
        int start = 0;
        int end = array.length -1;

        for (int i = 0; i < array.length; i++)   {
            int middle = (end - start)/2;
            if (array[i] == value)  {
                return true;
            }
            else if (array[middle] > value)  {
                end = middle - 1;
            }
            else    {
                start = middle + 1;
            }
        }
        return false;
    }
}
