/**
 * Created by LalinPethiyagoda on 12/03/2017.
 */
public class NumTwo implements Runnable {

    public double getTotalY() {
        return totalY;
    }

    public void setTotalY(double totalY) {
        this.totalY = totalY;
    }

    private double totalY =0;

    public double calculateY(){
        double y =0;
        for(int i=0; i<10; i++){
            y= y+  Math.random();
            System.out.println("in thread 2 calc y " + y + " i = " + i );
        }
        return y;
    }
    @Override
    public void run() {
        setTotalY(calculateY());
    }
}
