import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 01/11/2015.
 * Armstrong Number in Java: Armstrong number is a number that is equal to the sum of cubes of its digits for example 0, 1, 153, 370, 371, 407 etc.
 * Let's try to understand why 153 is an Armstrong number.
 * 153 = (1*1*1)+(5*5*5)+(3*3*3)
 * where:
 * (1*1*1)=1
 * (5*5*5)=125
 * (3*3*3)=27
 * So:
 * 1+125+27=153
 *
 *
 *
 *
 */
public class ArmstrongNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int armNumber = 0;
        int calculatedArmNumber=0;
        int nextDigit=0;

        System.out.println("Enter a possible Armstrong number ");
        armNumber=sc.nextInt();
        //get the length of a number
        final int number = armNumber;
        final int digits = number==0?1:(1 + (int)Math.floor(Math.log10(Math.abs(armNumber))));
        for(int i=0;i<digits;i++){
            nextDigit=armNumber%10;
            calculatedArmNumber=calculatedArmNumber + nextDigit*nextDigit*nextDigit;
            armNumber=armNumber /10;
            System.out.println(calculatedArmNumber);
        }

        if(number==calculatedArmNumber){
            System.out.println("Its an armstrong bumber");
        }

    }
}
