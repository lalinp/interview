/**
 * Created by LalinPethiyagoda on 26/10/2015.
 */
public class Dog extends Animal{

    public  Dog(){
        System.out.println("A new DOG is born mwaaaaahaha!");
    }
    @Override
    public void sleep() {

        System.out.println("Dog is sleeping zzzzzzzz ...");

    }
    @Override
    public void eat() {

        System.out.println("Dog is eating Om nom nom...");

    }
}
