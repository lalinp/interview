package Threads;

/**
 * Created by LalinPethiyagoda on 03/01/2016.
 */
public class ThreadRunner {
    public static void main(String[] args) {
        ThreadOne threadOne = new ThreadOne();
        ThreadTwo threadTwo= new ThreadTwo();

        threadOne.start();
        threadTwo.start();

    }
}
