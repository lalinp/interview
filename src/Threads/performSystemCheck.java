package Threads;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by LalinPethiyagoda on 28/12/2015.
 */
public class performSystemCheck implements Runnable {
    private String checkStatus;
    ReentrantLock lock = new ReentrantLock();

    public performSystemCheck(String checkStatus){
        this.checkStatus = checkStatus;
    }

    @Override
    public void run() {
        lock.lock();
            System.out.println("Running Thread "  + checkStatus);
        lock.unlock();
    }
}
