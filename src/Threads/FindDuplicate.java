package Threads;

import java.util.Arrays;

/**
 * Created by LalinPethiyagoda on 30/01/2016.
 */
public class FindDuplicate {
   static int[] numArray={1,4,5,2,7,6,13,14};

    public static void main(String[] args) {
        int repeatNumber =0;
        boolean found = false;
        Arrays.sort(numArray);
        for(int i=0; i<numArray.length-1; ++i){
            if(numArray[i]==numArray[i+1]){
                found=true;
                repeatNumber=numArray[i];
                break;
            }
        }
        if(found)
            System.out.println(repeatNumber);
        else
            System.out.println("no repeating numbers");


    }
}
