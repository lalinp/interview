/**
 * Created by LalinPethiyagoda on 28/10/2015.
 */
public class CheckPrime {
    public static void main(String[] args){
        int A, B;
        boolean result=solution(8);
        System.out.println(result);
    }
    public static boolean solution( int B) {
        boolean isPrime=true;
        for(int i=2; i< B; i++){
            if (B % i == 0) {
                isPrime=false;
                break;
            }
        }

        return isPrime;
    }
}
