import java.util.stream.Stream;

/**
 * Created by LalinPethiyagoda on 01/11/2015.
 */
public class CountCharacters {
    public static void main(String[] args) {
        int counter = 0;
        int result = 0;
        String input = "Todaaaay is Monday"; //count number of "a" on this String.

        for (int i = 0; i < input.length() - 1; i++) {
            char x = input.charAt(i);
            result = x == 'a' ? counter += 1 : counter;
        }
        System.out.println(result);

        int resultOne = (int) Stream.of(input).filter(x -> "a".equals(x)).count();



        Stream<String> awords = Stream.of(input);
        int abc = (int) Stream.of(input).map(w -> w.split("")).flatMap(x->Stream.of(x)).filter(eachChar->eachChar.equals("a")).count();
//Arrays::stream
        System.out.println(abc);
    }
}