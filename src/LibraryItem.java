import java.util.Date;

/**
 * Created by LalinPethiyagoda on 25/10/2015.
 */
public class LibraryItem {
    private String title="";
    private boolean checkedOut=false;
    private Date DateCheckedOut;
    private Date DateofReturn;
    private Date dateOfAcquisition;
    private float acquisitionCost=0;
    private float replacementCost=0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    public Date getDateCheckedOut() {
        return DateCheckedOut;
    }

    public void setDateCheckedOut(Date dateCheckedOut) {
        DateCheckedOut = dateCheckedOut;
    }

    public Date getDateofReturn() {
        return DateofReturn;
    }

    public void setDateofReturn(Date dateofReturn) {
        DateofReturn = dateofReturn;
    }

    public Date getDateOfAcquisition() {
        return dateOfAcquisition;
    }

    public void setDateOfAcquisition(Date dateOfAcquisition) {
        this.dateOfAcquisition = dateOfAcquisition;
    }

    public float getAcquisitionCost() {
        return acquisitionCost;
    }

    public void setAcquisitionCost(float acquisitionCost) {
        this.acquisitionCost = acquisitionCost;
    }

    public float getReplacementCost() {
        return replacementCost;
    }

    public void setReplacementCost(float replacementCost) {
        this.replacementCost = replacementCost;
    }


}