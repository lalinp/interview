package collections_package;

import java.util.Scanner;
import java.util.Stack;

/**
 * Created by LalinPethiyagoda on 01/01/2016.
 */
public class StackSolution {

    public static void main(String []argh)
    {
        Scanner sc = new Scanner(System.in);
        Stack<String> stack= new Stack<>();
        boolean isBalanced=false;

        String input=sc.next();
        //while (sc.hasNext()) {
            char[] x = input.toCharArray();
            for(Character y:x){

                if(y.toString().equals("(") || y.toString().equals("[") || y.toString().equals("{")){
                    stack.push(y.toString());
                }

                if(y.toString().equals(")") ){
                    isBalanced=stack.pop().equals("(")?true:false;
                }
                if(y.toString().equals("]") ){
                    isBalanced=stack.pop().equals("[")?true:false;
                }
                if(y.toString().equals("}") ){
                    isBalanced=stack.pop().equals("{")?true:false;
                }
            }


        //}

        System.out.println(isBalanced);

    }
}
