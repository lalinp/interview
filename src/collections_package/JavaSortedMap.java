package collections_package;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by LalinPethiyagoda on 29/10/2015.
 * Hashmaps dont maintain order
 * to maintain order in which you have typed in the data use a linked hashmap - same as a hashmap but has a doubly linked list. maintans order
 * it uses the hashcode of the object to maintain  items in the hashmap
 * hashmaps use the objects hashcode to store items in the hashmap
 *
 * the best:
 * if you want to dsiplay any random list in numerical ro alphabetical order, use a tree map
 */
public class JavaSortedMap {
    public static void main(String[] args) {
        Map<Integer, String> hashMap = new HashMap<Integer,String>();
        Map<Integer,String> linkedHashMap= new LinkedHashMap<Integer, String>(); // uses a doubly linked list to maintain order
        Map<Integer,String> treeMap= new TreeMap<Integer, String>();

        testMap(hashMap);
        System.out.println("***************");
        testMap(linkedHashMap);
        System.out.println("*************** The tree maps sort the keys in the natural order");
        testMap(treeMap);
    }

    public static void testMap(Map<Integer,String> map){
        map.put(0,"Giraffe");
        map.put(4,"Lion");
        map.put(11,"Tiger");
        map.put(2,"cat");
        map.put(1,"Dog");
        map.put(18,"Mouse");

        for(Integer key:map.keySet()){
            String value= map.get(key);
            System.out.println(key+ " : "+ value);
        }
    }
}
