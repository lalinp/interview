package collections_package;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 31/12/2015.
 */
public class HashMapSolution {

    public static void main(String[] args){
        Map<String,String> phoneBook = new HashMap<String, String>();
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        System.out.print("enter number of entries: ");

        int entriesInBook=sc.nextInt();

        for(int i=0; i< entriesInBook; ++i){
            System.out.print("enter name : ");
            String key_NameOfPerson=sc.next() + " " + sc.next();
            System.out.println("** Key Name " + key_NameOfPerson);
            //sc.nextLine();
            System.out.print("enter number : ");
            String value_phoneNUmber =sc.next();
            System.out.println("** phone " + value_phoneNUmber);


            if(value_phoneNUmber.length()!=11){
                flag=false;
            }
            while(flag=false){
                System.out.println("invalid number - re-enter");
                value_phoneNUmber=sc.nextLine();
                if (value_phoneNUmber.length()==11){
                    flag=true;
                }
            }
            phoneBook.put(key_NameOfPerson,value_phoneNUmber);

        }

        //search values

        System.out.println("=====================");
        System.out.println("Search for a person by their name to get the phone number");
        String searchValue = sc.next() + " " + sc.next();
        System.out.println(searchValue);
        for(String pbKey:phoneBook.keySet()){
               if(pbKey.equals(searchValue)){
                   System.out.println("Phone number for " + searchValue + " : " +  phoneBook.get(pbKey));
            }
        }
        System.out.println("========================================================================");


    }
}
