package collections_package;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by LalinPethiyagoda on 29/10/2015.
 * HashMaps use Key-value pairs
 */
public class JavaHashMap {
    public static void main(String[] args){
        Map<Integer,String> hashMap = new HashMap<Integer, String>();
        hashMap.put(5,"Apples");
        hashMap.put(6,"Oranges");
        hashMap.put(7,"Bananas");
        /**
         * once the above has been set
         * you have a look up table
         * that uses the key to
         * look up a value
         *
         * to look up a value, hshmapref.get(key) is used
         * it then returns the value
         *  as int primitive data types cannot be used here
         * java auto box them to its Integer complex type
         * Maps cannot have duplicate keys.
         * If you try to enter another value to an existing key, the old value
         * is replaced by the new value. key remains unchanged.
         * HashMaps are not sorted in anyway
         */

        String str = hashMap.get(7);
        System.out.println(str);
        hashMap.put(7,"grapes");

        //after
    Set<Integer> keys=hashMap.keySet();
        for(Integer keySetForMap:keys){
            System.out.println("moto " + keySetForMap);
            System.out.println("guzzi " + hashMap.get(keySetForMap));
        }
        str = hashMap.get(7);
        System.out.println(str);

        //iterating through HashMaps

        for(Map.Entry<Integer, String> entry: hashMap.entrySet()){
            int key = entry.getKey();
            String val= entry.getValue();

            System.out.println(key + " : " + val);
        }

    }
}
