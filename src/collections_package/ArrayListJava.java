package collections_package;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LalinPethiyagoda on 29/10/2015.
 */
public class ArrayListJava {
    public static void main(String[] args){
        List<Integer> numbers = new ArrayList<Integer>();
        numbers.add(10);
        numbers.add(11);
        numbers.add(10);

        for(Integer getNumbers : numbers){
            System.out.println(getNumbers);
        }

        //use remove() to remove items from an array list
        // this can get very slow, especially if you try to remove
        //items in the middle of the array list
        // in cases where there are a lot of items to be removed, use a LinkedList
    }
}

