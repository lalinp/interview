package collections_package;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by LalinPethiyagoda on 29/10/2015.
 */
public class JavaLinkedList {
    public static void main(String[] args) {
        //arraylists manage arrays internally
        // just a bunch of elements in an array
        // adding an item to the end or removing an item to the list is fairly fast
        //arraylsts, when created, initally have ten elements. if we want more,
        //a new arraylist of 20 elements is created and the new item as well as the existing ine are copied across
        //when adding or removing an item from the middle of an array list, the existing items
        // are moved either to make space or bridge the gap
        //which can be very slow for large data sets
        //so if adding and removing data from the end or near the end of a list, use arraylists
        //adding to anywhere else or removing from anywhere else, use linked lists
        List<Integer> arrayList = new ArrayList<Integer>();
        List<Integer> linkedList = new LinkedList<Integer>();

        doTimings("arrayList ", arrayList);
        doTimings("linkedList ", linkedList);


        /**
         * use an arraylist if you want to add remove items to the end of the list
         * use a linked list if you have to add remove items from anywhere else of the list
         * @param str
         * @param list
         */

    }

    private static void doTimings(String type, List<Integer> list) {

        long start = System.currentTimeMillis()  ;

        for (int i = 0; i <1E5; i++) {
            list.add(0,i);
        }

        long end = System.currentTimeMillis();


        System.out.println("time taken for " + type + " " + (end-start) + " in miliseconds" );

    }
}
