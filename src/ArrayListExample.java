import java.util.ArrayList;
import java.util.List;

/**
 * Created by LalinPethiyagoda on 02/12/2015.
 */

    public class ArrayListExample {
        public static void main(String args[]) {
      /*Creation of ArrayList: I'm going to add String
       *elements so I made it of string type */
            List<String> obj = new ArrayList<String>();

	  /*This is how
	   should be added to the array list*/
            obj.add("Harry");
            obj.add("Becky");
            obj.add("Liberty");
            obj.add("Lucas");
            obj.add("Jason");

	  /* Displaying array list elements */
            System.out.println("Currently the array list has following elements:"+obj);

	  /*Add element at the given index*/
            obj.add(0, "Luke");
            obj.add(1, "James");
            obj.add(1, "Kevin");
            obj.add(1, "J-Lo");
            obj.add(1, "Arun");


	  /*Remove elements from array list like this*/
            obj.remove("Becky");
            obj.remove("Harry");

            System.out.println("Current array list is:"+obj);

	  /*Remove element from the given index*/
            obj.remove(1);

            System.out.println("Current array list is:"+obj);
        }
    }



