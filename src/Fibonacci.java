/**
 * Created by LalinPethiyagoda on 22/10/2015.
 *
 * In mathematics, the Fibonacci numbers or Fibonacci series or Fibonacci sequence are the numbers in the
 * following integer sequence: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144...
 * By definition, the first two numbers in the Fibonacci sequence are 0 and 1,
 * and each subsequent number is the sum of the previous two.
 * */

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args){
        int numberOfFibo=0;

        int prev=0,current=1, next=0;
        Scanner sc = new Scanner(System.in);
        System.out.println("enter number for Fibonacci");
        numberOfFibo=sc.nextInt();

        for (int x=0; x<=numberOfFibo; x++){

            if(x==0){
                System.out.print(prev + " ");
                System.out.print(current + " ");
            }
            next= current+prev;
            System.out.print(next + " ");
            prev=current;
            current=next;
        }
    }
}
