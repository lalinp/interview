import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 28/10/2015.
 */
public class RangeOfPrimes {

    public static void main(String[] args){
        int primeCounter =0;
        int upperRange = 0;
        String primeNumbersIntheRange="";
        System.out.println("enter upper range");
        Scanner sc = new Scanner(System.in);
        upperRange = sc.nextInt();

        for(int i=1; i<=upperRange;i++){
            primeCounter=0;
            for(int j=i; j>=1;j--){
                if(i % j==0){
                    primeCounter=primeCounter+1;
                }
            }
            if(primeCounter==2){
                primeNumbersIntheRange=primeNumbersIntheRange + " " +i;
            }

        }

        System.out.println(primeNumbersIntheRange);
    }


}
