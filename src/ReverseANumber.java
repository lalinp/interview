import java.util.Scanner;

/**
 * Created by LalinPethiyagoda on 30/01/2016.
 */
public class ReverseANumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter a number to reverse");
        int number= sc.nextInt();
        String inReverse="";

        // convert to string

        String numINString = Integer.toString(number);
        for(int i=numINString.length()-1; i>=0; i--){
            inReverse= inReverse + numINString.charAt(i);
        }
        int numInRev = Integer.parseInt(inReverse);

        System.out.println(numInRev);
    }
}
