package LinkedListExample;

/**
 * Created by LalinPethiyagoda on 23/11/2015.
 */
public class Linked_List {
    private Link first; // ref to the first link on the list

    public void Linked_List(){ // constructor
        first = null; // no items on the list yet
    }

    public boolean isEmpty(){
        return (first==null); // true if list is empty
    }

    // other methods (adding to the list, removing etc

    public void insertFirst(String Name,int age,String favBand,boolean inARelationship){
        // make a  new link

        Link link = new Link(Name,age,favBand,inARelationship);

        if(first!=null) {
            link.setNextLink(first);//new object's next references the (old) first link
            first = link;// new object is placed at the start

        }
        else{
            first=link;
        }
    }

    public Link deleteFirst(){

        Link deletedLink = first;
        first = first.getNextLink(); // get the second list reference
        return deletedLink;
    }

    public void displayList(){

        System.out.print("list (first-->last): ");
        Link current = first;

        //loop
        while(current!=null){
            current.displayLink();// call method to display the data
            current=current.getNextLink();
        }

        System.out.println(" ");

    }

}
