package LinkedListExample;

/**
 * Created by LalinPethiyagoda on 17/11/2015.
 */
public class Node {

    private Node nextNode=null;
    private int dataValue=0; // change data type to object if you want to store ANY type of data

    public Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
    }

    public int getDataValue() {
        return dataValue;
    }

    public void setDataValue(int dataValue) {
        this.dataValue = dataValue;
    }


    public Node(){

    }
    // overloaded constructor if both values are present
    public Node (int data, Node node){
        dataValue = data;
        nextNode = node;
    }
    //another overloaded constructor
    public Node (int data){
        this();//constructor chaining!
        dataValue = data;
        nextNode = null;
    }


}
