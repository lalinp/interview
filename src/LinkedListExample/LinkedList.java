package LinkedListExample;

/**
 * Created by LalinPethiyagoda on 17/11/2015.
 */
public class LinkedList {

    // the linked list is a collection of nodes...
    // you need to create the first node, link it to a variable (which then becomes its starting point)

    //listHead is the start variable
    private Node listHead;
    private int numberOfNodesInTheList = 0;

    //constructor to set the start variable with an empty Node (to start with)
    // once that is done, we can use the set() method of the node to set the value and the next reference

    public LinkedList() {
        listHead = null; // empty list
    }

    public boolean addFirstNode(int data) {
        listHead = new Node();//new node is linked to the variable!
        listHead.setDataValue(data);//new node has some data
        listHead.setNextNode(null);// the new node points to no other node at the moment
        numberOfNodesInTheList = 1;
        return true;
    }

    public void addNextNode(int dataItem) {
        Node currentItem = listHead;
        while (currentItem.getNextNode() != null) {
            currentItem = currentItem.getNextNode();
        }
        currentItem.setNextNode(new Node(dataItem));
        numberOfNodesInTheList++;
    }

    /**
     * removes the first occaurance of a number in the list
     */
    public boolean removeNode(int dataValue) {
        return listHead == null ? false : remove(dataValue);
    }

    public boolean remove(int data) {
        Node currentItem = listHead;
        //only one node on the list
        if(currentItem.getNextNode()==null) {
            //we have found the value we are looking for
            if (currentItem.getDataValue() == data) {
                listHead = null;
                numberOfNodesInTheList--;
                return true;
            } else return false;
        }
        else {
            //keep looking for the value to delete
            while (currentItem.getNextNode() != null) {
                if (currentItem.getDataValue() != data) {
                    currentItem = currentItem.getNextNode();
                }
                currentItem.setNextNode(currentItem.getNextNode().getNextNode());
                numberOfNodesInTheList--;

            }return true;
        }
    }

    public boolean addNode(int data){
        return listHead==null? addFirstNode(data):add(data);
    }

    public boolean add(int dataItem){
        Node currentItem = listHead;
        while(currentItem.getNextNode()!=null){
            currentItem = currentItem.getNextNode();
        }
        Node temp= new Node(dataItem);
        currentItem.setNextNode(temp);
        return true;
    }

}
