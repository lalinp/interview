package LinkedListExample;

/**
 * Created by LalinPethiyagoda on 24/11/2015.
 */
public class LinkedList_Controller {
    public static void main(String[] args) {
        Linked_List studentDataList= new Linked_List();
        studentDataList.insertFirst("Wendy",100,"One Direction",true);//fourth
        studentDataList.insertFirst("Peter Pan",10,"Little Mix",true);//third
        studentDataList.insertFirst("Cap'n Hook",50,"Cheeky Girls",true); //second
        studentDataList.insertFirst("Smee",100,"Cheeky Girls",true);//first link

        studentDataList.displayList();
    }

}
